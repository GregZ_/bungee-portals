package me.gregz_.bungee_portals;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import me.gregz_.bungee_portals.Commands.CommandBPortals;
import me.gregz_.bungee_portals.Listeners.EventListener;
import me.gregz_.bungee_portals.portal.PortalManager;

public class BungeePortals extends JavaPlugin {

	public FileConfiguration portalsFile = null;

	private PortalManager portalManager;
	
	private ItemStack selector = new ItemStack(Material.STICK);

	public void onEnable() {
		ItemMeta im = selector.getItemMeta();
		im.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&aBungeePortals Wand"));
		selector.setItemMeta(im);
		
		getCommand("BPortals").setExecutor(new CommandBPortals(this));
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		loadConfigurationFiles();
		portalManager = new PortalManager(this);
		getServer().getPluginManager().registerEvents(new EventListener(this), this);
	}

	public void loadConfigurationFiles() {
		if (!new File(getDataFolder(), "config.yml").exists()) {
			saveDefaultConfig();
		}
		if (!new File(getDataFolder(), "portals.yml").exists()) {
			saveResource("portals.yml", false);
		}
		portalsFile = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "portals.yml"));
	}

	// API	
	public PortalManager getPortalManager() {
		return portalManager;
	}
	
	public ItemStack getSelector() {
		return selector;
	}
}
