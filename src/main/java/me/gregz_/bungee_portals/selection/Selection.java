package me.gregz_.bungee_portals.selection;

import org.bukkit.Location;

public class Selection {

	private Location pos1;
	private Location pos2;

	private Location min;
	private Location max;

	public Selection(Location pos1, Location pos2) {
		this.pos1 = pos1;
		this.pos2 = pos2;
		redefineLocations();
	}

	public Selection() {

	}

	private void redefineLocations() {
		if (pos1 != null && pos2 != null) {
			min = new Location(pos1.getWorld(), 
					Math.min(pos1.getBlockX(), pos2.getBlockX()),
					Math.min(pos1.getBlockY(), pos2.getBlockY()), 
					Math.min(pos1.getBlockZ(), pos2.getBlockZ()));
			max = new Location(pos1.getWorld(), 
					Math.max(pos1.getBlockX(), pos2.getBlockX()), 
					Math.max(pos1.getBlockY(), pos2.getBlockY()), 
					Math.max(pos1.getBlockZ(), pos2.getBlockZ()));
		}
	}

	public Selection setPos1(Location loc) {
		pos1 = loc;
		redefineLocations();
		return this;
	}

	public Selection setPos2(Location loc) {
		pos2 = loc;
		redefineLocations();
		return this;
	}

	public Location getMinimumLocation() {
		return min;
	}

	public Location getMaximumLocation() {
		return max;
	}

	public int getLength() {
		return max.getBlockX() - min.getBlockX() + 1;
	}

	public int getWidth() {
		return max.getBlockZ() - min.getBlockZ() + 1;
	}

	public int getHeight() {
		return max.getBlockY() - min.getBlockY() + 1;
	}

	public int getSize() {
		return getLength() * getWidth() * getHeight();
	}

	public boolean isFullDefined() {
		return min != null && max != null;
	}

}
