package me.gregz_.bungee_portals.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.gregz_.bungee_portals.BungeePortals;

public class CommandBPortals implements CommandExecutor {

	private BungeePortals plugin;

	public CommandBPortals(BungeePortals plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (!commandLabel.equalsIgnoreCase("BPortals")) {
			return false;
		}
		if (!sender.hasPermission("BungeePortals.command.BPortals")) {
			sender.sendMessage(ChatColor.RED + "You do not have permission to use that command.");
			return true;
		}
		if (args.length == 0) {
			sender.sendMessage(ChatColor.BLUE + "RoyalMC BungeePortals v" + plugin.getDescription().getVersion() + " by GregZ_");
			sender.sendMessage(ChatColor.GREEN + "/BPortals selector " + ChatColor.RED + "Get the point selector item.");
			sender.sendMessage(ChatColor.GREEN + "/BPortals reload " + ChatColor.RED + "Reload all files and data.");
			sender.sendMessage(ChatColor.GREEN + "/BPortals clear " + ChatColor.RED + "Clear selection.");
			sender.sendMessage(ChatColor.GREEN + "/BPortals create <name> <destination> " + ChatColor.RED + "Create portals.");
			sender.sendMessage(ChatColor.GREEN + "/BPortals remove <name> " + ChatColor.RED + "Remove portals.");
			return true;
		} else {
			if (args[0].equalsIgnoreCase("reload")) {
				plugin.loadConfigurationFiles();
				plugin.getPortalManager().loadAll();
				sender.sendMessage(ChatColor.GREEN + "All configuration files and data have been reloaded.");
			} 
			
			else if (args[0].equalsIgnoreCase("selector")) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(ChatColor.RED + "Only players can use that command.");
					return true;
				}
				Player p = (Player) sender;
				p.getInventory().addItem(plugin.getSelector());
				sender.sendMessage(ChatColor.GREEN + "Have a selector item :)");
			} 

			else if (args[0].equalsIgnoreCase("clear")) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(ChatColor.RED + "Only players can use that command.");
					return true;
				}
				plugin.getPortalManager().removeSelection(sender.getName());
				sender.sendMessage(ChatColor.GREEN + "Selection cleared.");
			} 

			else if (args[0].equalsIgnoreCase("create")) {
				if (args.length < 3) {
					sender.sendMessage(ChatColor.RED + "Insufficient arguments.");
					return true;
				}
				if (sender instanceof Player) {
					plugin.getPortalManager().createPortal((Player) sender, args[1], args[2]);
				} else {
					sender.sendMessage(ChatColor.RED + "Only players can use that command.");
				}
			} 

			else if (args[0].equalsIgnoreCase("remove")) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(ChatColor.RED + "Only players can use that command.");
					return true;
				}
				if (args.length < 2) {
					sender.sendMessage(ChatColor.RED + "Insufficient arguments.");
					return true;
				}
				plugin.getPortalManager().removePortal((Player) sender, args[1]);
			} else {
				sender.sendMessage(ChatColor.RED + "Type /BPortals for help!");
				return true;
			}
		}
		return true;
	}
}
