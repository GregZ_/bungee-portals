package me.gregz_.bungee_portals.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.gregz_.bungee_portals.BungeePortals;
import me.gregz_.bungee_portals.portal.Portal;

public class EventListener implements Listener {

	private BungeePortals plugin;

	public EventListener(BungeePortals plugin) {
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerPortalEnter(PlayerPortalEvent event) {
		Portal portal = plugin.getPortalManager().getPortal(event.getPlayer().getLocation());
		if (portal == null) {
			System.out.println("portal is null");
			return;
		}
		
		event.useTravelAgent(false);
		event.setCancelled(true);
		
		if (!portal.canUse(event.getPlayer())) {
			System.out.println("no portal perms");
			return;
		}
		
		plugin.getPortalManager().sendTo(event.getPlayer(), portal);
		System.out.println("sending to server");
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event) {
		plugin.getPortalManager().removePending(event.getPlayer().getName());
		plugin.getPortalManager().removeSelection(event.getPlayer().getName());
	}
	
	@EventHandler
	public void onPlayerKick(PlayerKickEvent event) {	
		plugin.getPortalManager().removePending(event.getPlayer().getName());
		plugin.getPortalManager().removeSelection(event.getPlayer().getName());
	}
	
	@EventHandler
	public void onPointSelect(PlayerInteractEvent event) {
		if(event.getItem() == null) {
			return;
		}
		
		if(!event.getItem().equals(plugin.getSelector())) {
			return;
		}
		
		event.setCancelled(true);
		if (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
			plugin.getPortalManager().setPos1(event.getPlayer().getName(), event.getClickedBlock().getLocation());
		} else if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			plugin.getPortalManager().setPos2(event.getPlayer().getName(), event.getClickedBlock().getLocation());
		}
	}
}
