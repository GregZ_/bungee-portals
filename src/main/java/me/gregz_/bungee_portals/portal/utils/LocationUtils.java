package me.gregz_.bungee_portals.portal.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * 
 * This is a general Location serialisation and parsing class.
 * 
 * @author GregZ_
 *
 */
public class LocationUtils {

	/**
	 * This is a simple method for parsing strings to locations.
	 * 
	 * @param s
	 *            The string value of a serialised location which is to be converted back into a location object.
	 * @return The parsed location if parsing was successful or null if it was not.
	 */
	public Location parseLocation(String s) {
		String[] split = s.split(",");
		Location loc = null;

		try {
			World world = Bukkit.getWorld(split[0]);
			int x = Integer.parseInt(split[1]);
			int y = Integer.parseInt(split[2]);
			int z = Integer.parseInt(split[3]);
			loc = new Location(world, x, y, z);
		} catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
			System.err.println("\u001B[30;1m[\u001B[32;1mBungeePortals\u001B[30;1m] \u001B[31;1mCannot parse location from string: " + s + " - Mabye this is the reason: " + e.toString() + "\u001B[0m");
		}

		return loc;
	}

	/**
	 * This is a simple method for serialising locations to strings.
	 * 
	 * @param l
	 *            The location object that is to be serialised.
	 * @param exact
	 *            Whether or not to return an exact serialisation of the string (meaning that it includes pitch, yaw and the exact x, y, z of the location [this is the case if exact is true]). If exact is false then a string containing only the world and the block x, y, z values will be returned.
	 * @return A serialised string for the location provided or null if no location was provided.
	 */
	public String serializeLocation(Location l) {
		if (l != null) {
			String key = l.getWorld().getName() + ",";
			key += l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ();
			return key;
		}
		return null;
	}
}