package me.gregz_.bungee_portals.portal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.gregz_.bungee_portals.BungeePortals;
import me.gregz_.bungee_portals.portal.utils.LocationUtils;
import me.gregz_.bungee_portals.selection.Selection;

public class PortalManager {

	private BungeePortals plugin;
	private List<Portal> portals = new ArrayList<Portal>();
	private List<String> pending = new ArrayList<String>();
	private HashMap<String, Selection> selections = new HashMap<String, Selection>();
	private LocationUtils util = new LocationUtils();

	public PortalManager(BungeePortals plugin) {
		this.plugin = plugin;
		loadAll();
	}

	public void loadAll() {
		portals = new ArrayList<Portal>();
		int loaded = 0;

		if (plugin.portalsFile.contains("Portals")) {
			for (String key : plugin.portalsFile.getConfigurationSection("Portals.").getKeys(false)) {
				if (load(key))
					loaded++;
			}
		}

		System.out.println("\u001B[30;1m[\u001B[32;1mBungeePortals\u001B[30;1m] \u001B[36;1m" + loaded + (loaded == 1 ? " portal" : " portals") + " loaded!\u001B[0m");
	}

	public boolean load(String key) {
		if (getPortal(key) != null) {
			System.err.println("\u001B[30;1m[\u001B[32;1mBungeePortals\u001B[30;1m] \u001B[31;1mPortal " + key + " is already loaded!\u001B[0m");
			return false;
		}

		String path = "Portals." + key;

		if (!plugin.portalsFile.contains(path)) {
			System.err.println("\u001B[30;1m[\u001B[32;1mBungeePortals\u001B[30;1m] \u001B[31;1mPortal " + key + " does not exist!\u001B[0m");
			return false;
		}

		path += ".";

		if (!plugin.portalsFile.contains(path + "Min")) {
			System.err.println("\u001B[30;1m[\u001B[32;1mBungeePortals\u001B[30;1m] \u001B[31;1mPortal " + key + " has no minimum location!\u001B[0m");
			return false;
		}

		if (!plugin.portalsFile.contains(path + "Max")) {
			System.err.println("\u001B[30;1m[\u001B[32;1mSurvivalGames\u001B[30;1m] \u001B[31;1m" + "Lobby " + key + " has no maximum location![0m");
			return false;
		}

		if (!plugin.portalsFile.contains(path + "Destination")) {
			System.err.println("\u001B[30;1m[\u001B[32;1mSurvivalGames\u001B[30;1m] \u001B[31;1m" + "Lobby " + key + " has no destination![0m");
			return false;
		}

		portals.add(new Portal(key, util.parseLocation(plugin.portalsFile.getString(path + "Min")), util.parseLocation(plugin.portalsFile.getString(path + "Max")), plugin.portalsFile.getString(path + "Destination")));

		return true;
	}

	public void sendTo(Player p, Portal portal) {
		if (pending.contains(p.getName())) {
			return;
		}
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Connect");
		out.writeUTF(portal.getDestination());
		p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());

		pending.add(p.getName());
	}

	public void removePending(String name) {
		pending.remove(name);
	}

	public List<Portal> getPortals() {
		return portals;
	}

	public Portal getPortal(Location loc) {
		for (Portal portal : portals) {
			if (portal.containsLocation(loc)) {
				return portal;
			}
		}
		return null;
	}

	private Portal getPortal(String name) {
		for (Portal portal : portals) {
			if (portal.getName().equalsIgnoreCase(name)) {
				return portal;
			}
		}
		return null;
	}
	
	public HashMap<String, Selection> getSelections() {
		return selections;
	}
	
	public Selection getSelection(String name) {
		Selection selection = selections.get(name);
		if (selection == null) {
			selection = new Selection();
			selections.put(name, selection);
		}
		return selection;
	}
	
	public void setPos1(String name, Location pos) {
		Selection selection = selections.get(name);
		if (selection == null) {
			selection = new Selection();
		}
		selections.put(name, selection.setPos1(pos));
	}
	
	public void setPos2(String name, Location pos) {
		Selection selection = selections.get(name);
		if (selection == null) {
			selection = new Selection();
		}
		selections.put(name, selection.setPos2(pos));
	}

	public void removeSelection(String name) {
		selections.remove(name);
	}
	
	public boolean createPortal(Player p, String name, String destination) {
		if (!getSelection(p.getName()).isFullDefined()) {
			p.sendMessage(ChatColor.RED + "You must set the corner points of the portal!");
			return false;
		}
		if (plugin.portalsFile.contains("Portals." + name)) {
			p.sendMessage(ChatColor.RED + "The portal " + name + " already exists!");
			return false;
		}
		
		plugin.portalsFile.set("Portals." + name + ".Min", util.serializeLocation(getSelection(p.getName()).getMinimumLocation()));
		plugin.portalsFile.set("Portals." + name + ".Max", util.serializeLocation(getSelection(p.getName()).getMaximumLocation()));
		plugin.portalsFile.set("Portals." + name + ".Destination", destination);
		
		try {
			plugin.portalsFile.save(new File(plugin.getDataFolder(), "portals.yml"));
		} catch (IOException e) {
			p.sendMessage(ChatColor.RED + "An error occured when saving the portal data file! Maybe this is why: " + e.toString());
			e.printStackTrace();
			return false;
		}
		
		portals.add(new Portal(name, getSelection(p.getName()).getMinimumLocation(), getSelection(p.getName()).getMaximumLocation(), destination));
		p.sendMessage(ChatColor.GREEN + "Successfully created and loaded portal " + name + "!");
		p.sendMessage(ChatColor.GREEN + "It contains " + getSelection(p.getName()).getSize() + " blocks!");
		return true;
	}
	
	public boolean removePortal(Player p, String name) {
		if (plugin.portalsFile.contains("Portals." + name)) {
			p.sendMessage(ChatColor.RED + "The portal " + name + " does not exist!");
			return false;
		}
		
		plugin.portalsFile.set("Portals." + name, null);
		Portal portal = getPortal(name);
		
		if (portal != null) {
			portals.remove(portal);
		}
		
		try {
			plugin.portalsFile.save(new File(plugin.getDataFolder(), "portals.yml"));
		} catch (IOException e) {
			p.sendMessage(ChatColor.RED + "An error occured when saving the portal data file! Maybe this is why: " + e.toString());
			e.printStackTrace();
			return false;
		}
		p.sendMessage(ChatColor.GREEN + "Successfully removed portal " + name + "!");
		return true;
	}
}
