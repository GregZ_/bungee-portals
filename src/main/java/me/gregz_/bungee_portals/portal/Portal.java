package me.gregz_.bungee_portals.portal;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Portal {
	
	private String name;
	private Location min;
	private Location max;
	private String destination;
	
	public Portal(String name, Location min, Location max, String destination) {
		this.name = name;
		this.min = min;
		this.max = max;
		this.destination = destination;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean containsLocation(Location loc) {
		double x = loc.getX();
		double y = loc.getY();
		double z = loc.getZ();
		System.out.println((loc.getWorld().equals(min.getWorld()) && x >= min.getX() && x <= max.getX() && y >= min.getY() && y <= max.getY() && z >= min.getZ() && z <= max.getZ()));
		return (loc.getWorld().equals(min.getWorld()) && x >= min.getX() && x <= max.getX() && y >= min.getY() && y <= max.getY() && z >= min.getZ() && z <= max.getZ());
	}
	
	public boolean canUse(Player p) {
		return p.hasPermission("BungeePortals.portal." + name) || p.hasPermission("BungeePortals.portal.*");
	}
	
	public String getDestination() {
		return destination;
	}
}
